//
// Created by bogdan on 24/02/2021.
//

#ifndef GAMEJAM_TRAINING_SplashScreen_H
#define GAMEJAM_TRAINING_SplashScreen_H

#include <AnimationPlayer.hpp>
#include <Button.hpp>
#include <Control.hpp>
#include <Godot.hpp>
#include <InputEvent.hpp>
#include <InputEventKey.hpp>
#include <InputMap.hpp>
#include <Ref.hpp>
#include <ResourceLoader.hpp>
#include <SceneTree.hpp>
#include <Viewport.hpp>

namespace godot {

class SplashScreen : public Control {
	GODOT_CLASS(SplashScreen, Control);

public:
	static void _register_methods();
	void _init();
	void _ready();
	void _exit();
};

} // namespace godot
#endif //GAMEJAM_TRAINING_SplashScreen_H
