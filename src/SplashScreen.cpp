//
// Created by bogdan on 24/02/2021.
//

#include "SplashScreen.h"

using namespace godot;

void SplashScreen::_init() {
}

void SplashScreen::_ready() {
}

void SplashScreen::_exit() {
	get_tree()->quit();
}

void SplashScreen::_register_methods() {
	register_method("_init", &SplashScreen::_init);
	register_method("_ready", &SplashScreen::_ready);
	register_method("_exit", &SplashScreen::_exit);
}
